using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    private Vector2 _velocity;
    
    private void Update()
    {
        _velocity = Vector2.up * 8;
        transform.Translate(_velocity * Time.deltaTime);
    }
}
