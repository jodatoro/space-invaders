using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvaderTroopMovement : MonoBehaviour
{
    [SerializeField] private Vector2 _initialVelocity;
    private Vector2 _velocity;
    private Rigidbody2D _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _velocity = _initialVelocity;
    }

    private void Update()
    {
        _rigidbody.velocity = _velocity;
    }
    
    public void SetDirection(Directions direction)
    {
        switch (direction)
        {
            case Directions.Right:
                _velocity = _initialVelocity;
                break;
            
            case Directions.Left:
                _velocity = new Vector2(-_initialVelocity.x, _initialVelocity.y);
                break;
        }
    }
}
