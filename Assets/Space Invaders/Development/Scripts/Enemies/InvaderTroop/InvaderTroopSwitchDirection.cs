using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvaderTroopSwitchDirection : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("SideBorder"))
        {
            Directions direction = Directions.Left;

            if (transform.position.x > 0)
                direction = Directions.Left;
            else
                direction = Directions.Right;

            transform.parent.GetComponent<InvaderTroopMovement>().SetDirection(direction);
        }
    }
}
