using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvaderDeath : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("PlayerBullet"))
        {
            ScoreCounter score = GameObject.FindObjectOfType<ScoreCounter>();
            score.Add(13);
            
            Destroy(col.gameObject);
            Destroy(gameObject);
        }
    }
}
