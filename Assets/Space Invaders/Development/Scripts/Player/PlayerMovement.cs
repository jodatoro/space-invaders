using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D _rigidbody;
    
    // Atributos para la generación de movimiento.
    private Vector2 _direction;
    private const int SpeedMult = 8;

    private void Start()
    {
        // Recuperamos rigidbody y lo almacenamos en una variable.
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        // Recuperamos las entradas del teclado y las usaremos como dirección del movimiento.
        _direction = new Vector2(Input.GetAxis("Horizontal"), 0);
        
        // Establecemos que la velocidad es la dirección multiplicado por la rapidez.
        _rigidbody.velocity = _direction * SpeedMult;
    }
}
