using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class PlayerShooter : MonoBehaviour
{
    [SerializeField] private GameObject bullet;
    private readonly Vector3 _positionGaf = new Vector3(0, 0.4f);

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(bullet, transform.position + _positionGaf, Quaternion.Euler(Vector3.zero));
        }
    }
}
